package com.timewastingguru.otpshka.domain.interactors

import android.app.Notification
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.domain.providers.SmsConversationsProvider
import com.timewastingguru.otpshka.domain.providers.TransferPointProvider
import com.timewastingguru.otpshka.presentation.SendSmsWorker
import com.timewastingguru.otpshka.presentation.TransferService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*

class ReceiveSmsInteractor(

    val smsConversationsProvider: SmsConversationsProvider,
    val pointProvider: TransferPointProvider) {


    fun onSmsReceived(context: Context, address: String, text: String, date: Date) {
        GlobalScope.launch {
            val smsId = smsConversationsProvider.onSmsReceived(text = text, address = address, date = date)
            val sms = smsConversationsProvider.getSmsById(smsId)
            pointProvider.getPointsByAddress(address).collect {
                it.forEach { point ->
                    pointProvider.addTransfer(sms, point)
                }

                SendSmsWorker.start(context)
//                TransferService.start(context)
            }
        }
    }

}