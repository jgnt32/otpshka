package com.timewastingguru.otpshka.domain.providers

import com.timewastingguru.otpshka.domain.entities.Sms
import kotlinx.coroutines.flow.Flow
import java.util.*

interface SmsConversationsProvider {

    fun getSmsGroupedByAddress(): Flow<List<Sms>>

    fun getAllSmsByAddress(address: String): Flow<List<Sms>>

    suspend fun importSms(list: List<Sms>)

    fun count(): Flow<Int>

    suspend fun getSmsById(id: Long): Sms

    suspend fun onSmsReceived(address: String, text:String, date: Date): Long

}