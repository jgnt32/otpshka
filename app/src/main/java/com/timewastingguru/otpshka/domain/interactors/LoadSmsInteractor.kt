package com.timewastingguru.otpshka.domain.interactors

import com.timewastingguru.otpshka.presentation.SmsProvider
import com.timewastingguru.otpshka.domain.entities.Conversation
import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.domain.providers.SmsConversationsProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.transform

class LoadSmsInteractor(
    val smsConversationsProvider: SmsConversationsProvider,
    val smsProvider: SmsProvider
) {


    fun loadSms(): Flow<List<Sms>> {
        return smsConversationsProvider.count().transform { count ->

            if (count == 0) {
                smsConversationsProvider.importSms(smsProvider.readAllSms())
            }

            this.emitAll(smsConversationsProvider.getSmsGroupedByAddress())
        }
    }

}