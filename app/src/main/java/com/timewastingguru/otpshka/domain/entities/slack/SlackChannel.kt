package com.timewastingguru.otpshka.domain.entities.slack

data class SlackChannel(
    val name: String, val id: String)