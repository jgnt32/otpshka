package com.timewastingguru.otpshka.domain.entities

class Transfer(val id: Long,
               val sms: Sms,
               val point: TransferPoint)