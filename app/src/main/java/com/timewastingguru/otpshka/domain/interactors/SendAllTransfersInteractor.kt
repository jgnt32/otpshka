package com.timewastingguru.otpshka.domain.interactors

import com.timewastingguru.otpshka.domain.providers.TransferPointProvider
import com.timewastingguru.otpshka.domain.providers.TransferProvider
import kotlinx.coroutines.flow.firstOrNull

class SendAllTransfersInteractor(val transferPointProvider: TransferPointProvider,
                                 val transferProvider: TransferProvider) {

    suspend fun sendAllTransfers() {

        val transfers = transferPointProvider.getAllTransfers().firstOrNull()
        transfers?.forEach { transfer ->
                transferProvider.transfer(transfer.sms, transfer.point)
                transferPointProvider.deleteTransfer(transfer.id)
        }

    }


}