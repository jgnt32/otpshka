package com.timewastingguru.otpshka.domain.entities

import java.util.*

data class Sms(
    val id: Long,
    val text: String,
    val address: String,
    val date: Date
)