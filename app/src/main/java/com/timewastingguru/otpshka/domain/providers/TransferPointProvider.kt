package com.timewastingguru.otpshka.domain.providers

import com.timewastingguru.otpshka.domain.entities.*
import kotlinx.coroutines.flow.Flow

interface TransferPointProvider {

    fun getAll(): Flow<List<TransferPoint>>

    fun getExcept(address: String):Flow<List<TransferPoint>>

    fun getById(id: Long): Flow<TransferPoint>

    suspend fun add(title: String, type: TransferType, payload: Map<String, Any>)

    suspend fun delete(point: TransferPoint)

    suspend fun addLink(address: String, point: TransferPoint)

    suspend fun breakLink(address: String, point: TransferPoint)

    suspend fun breakAllLinks(address: String)

    fun getAllLinks(): Flow<List<TransferConfig>>

    suspend fun getAllTransfers(): Flow<List<Transfer>>

    fun getPointsByAddress(address: String): Flow<List<TransferPoint>>

    suspend fun addTransfer(sms: Sms, transferPoint: TransferPoint)

    suspend fun deleteTransfer(id: Long)

}