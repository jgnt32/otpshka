package com.timewastingguru.otpshka.domain.providers

import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.domain.entities.TransferPoint

interface TransferProvider {

    suspend fun transfer(sms: Sms, transferPoint: TransferPoint)

}