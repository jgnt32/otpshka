package com.timewastingguru.otpshka.domain.interactors

import com.timewastingguru.otpshka.domain.entities.TransferPoint
import com.timewastingguru.otpshka.domain.entities.TransferType
import com.timewastingguru.otpshka.domain.providers.TransferPointProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking

class TransferPointsInteractor(val provider: TransferPointProvider) {

    fun get(): Flow<List<TransferPoint>> {
        return provider.getAll()
    }

    fun getForNotThisAddress(address: String): Flow<List<TransferPoint>>{
        return provider.getExcept(address)
    }

    fun getById(id: Long): Flow<TransferPoint> {
        return provider.getById(id)
    }

    fun getByAddress(address: String): Flow<List<TransferPoint>> {
        return provider.getPointsByAddress(address)
    }

    fun add(title: String, type: TransferType, payload: Map<String, Any>) {
        runBlocking {
            provider.add(title, type, payload)
        }
    }

    fun delete(point: TransferPoint) {
        runBlocking {
            provider.delete(point)
        }
    }

    fun addLink(address: String, point: TransferPoint) {
        runBlocking {
            provider.addLink(address, point)
        }
    }

    fun saveLinks(address: String, points: List<TransferPoint>) {

        runBlocking {
            provider.breakAllLinks(address)
            points.forEach {
                provider.addLink(address, it)
            }
        }

    }

    fun breakLink(address: String, point: TransferPoint) {
        runBlocking {
            provider.breakLink(address, point)
        }
    }


}