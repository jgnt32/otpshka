package com.timewastingguru.otpshka.domain.interactors

import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.domain.providers.SmsConversationsProvider
import kotlinx.coroutines.flow.Flow

class LoadConversationInteractor(val smsConversationsProvider: SmsConversationsProvider) {

    fun loadConverations(address: String): Flow<List<Sms>> {
        return smsConversationsProvider.getAllSmsByAddress(address)
    }

}