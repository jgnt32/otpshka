package com.timewastingguru.otpshka.domain.entities

class TransferConfig(val address: String, val transferPoints: List<TransferPoint>)