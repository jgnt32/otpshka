package com.timewastingguru.otpshka.domain.entities

data class TransferPoint(
    val id: Long,
    val name: String,
    val payload: Map<String, Any>)


enum class TransferType {
    SLACK
}


