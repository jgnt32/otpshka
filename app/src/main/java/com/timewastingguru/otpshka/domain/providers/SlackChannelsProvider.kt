package com.timewastingguru.otpshka.domain.providers

import com.timewastingguru.otpshka.domain.entities.slack.SlackChannel

interface SlackChannelsProvider {

    suspend fun getChannels(token: String): List<SlackChannel>

}