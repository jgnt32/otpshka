package com.timewastingguru.otpshka.domain.providers

import com.timewastingguru.otpshka.domain.entities.TransferPoint

interface SlackTransferPoint {

    fun sendMessage(transferPoint: TransferPoint) {

    }

}