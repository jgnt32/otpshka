package com.timewastingguru.otpshka.domain.providers

import com.timewastingguru.otpshka.domain.entities.TransferPoint
import kotlinx.coroutines.flow.Flow

interface SmsTransfer {

    fun sendSms(sms: String, point: TransferPoint): Flow<Boolean>

}