package com.timewastingguru.otpshka.domain.entities


data class Conversation(
    val address: String,
    val messages: List<String>,
)