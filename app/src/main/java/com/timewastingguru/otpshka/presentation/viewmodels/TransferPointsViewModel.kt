package com.timewastingguru.otpshka.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timewastingguru.otpshka.domain.entities.TransferPoint
import com.timewastingguru.otpshka.presentation.App
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TransferPointsViewModel: ViewModel() {

    private val interactor = App.provider.transferPointInteractor

    val points = MutableLiveData<List<TransferPoint>>()

    fun setAddress(address: String) {
        viewModelScope.launch {
            interactor.getForNotThisAddress(address).collect {
                points.value = it
            }
        }
    }

}