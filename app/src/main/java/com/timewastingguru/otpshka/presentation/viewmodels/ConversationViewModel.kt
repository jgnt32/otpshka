package com.timewastingguru.otpshka.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.presentation.App
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ConversationViewModel: ViewModel() {

    val smsList = MutableLiveData<List<Sms>>()

    private val interactor = App.provider.conversationInteractor

    fun loadConversation(address: String) {
        viewModelScope.launch {
            interactor.loadConverations(address).collect {
                smsList.value = it
            }
        }

    }

}