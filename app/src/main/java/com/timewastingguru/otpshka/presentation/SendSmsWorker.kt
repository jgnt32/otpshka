package com.timewastingguru.otpshka.presentation

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.work.*
import androidx.work.impl.utils.futures.SettableFuture
import com.google.common.util.concurrent.ListenableFuture
import com.timewastingguru.otpshka.R
import java.lang.Exception

class SendSmsWorker(context: Context, workerParams: WorkerParameters) :
    CoroutineWorker(context, workerParams) {

    companion object {

        private const val CHANNEL_ID = "SendingSmsChannel"

        fun start(context: Context) {
            val request = OneTimeWorkRequest.Builder(SendSmsWorker::class.java)
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build()

            WorkManager.getInstance(context).enqueueUniqueWork("send_sms", ExistingWorkPolicy.KEEP, request)

        }

    }

    override suspend fun getForegroundInfo(): ForegroundInfo {
        return createForegroundInfo(applicationContext)
    }


    override suspend fun doWork(): Result {

        return try {
            App.provider.sendAllTransfersIteractor.sendAllTransfers()
            Result.success()
        } catch (e: Exception) {
            e.printStackTrace()
            Result.retry()
        }
    }

    fun createForegroundInfo(context: Context): ForegroundInfo {

        return ForegroundInfo(12, createNotification(context))
    }

    fun createNotification(context: Context): Notification {
        val pendingIntent: PendingIntent =
            Intent(context, MainActivity::class.java).let { notificationIntent ->
                PendingIntent.getActivity(context, 0, notificationIntent, 0)
            }

        val notification: Notification = NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentTitle("Sending sms")
            .setContentText("to slack")
            .setSmallIcon(R.drawable.ic_transfer)
            .setContentIntent(pendingIntent)
            .build()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(context)
        }

        return notification

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context) {
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (manager.getNotificationChannel(CHANNEL_ID) == null) {
            val channel = NotificationChannel(CHANNEL_ID, "Sending sms", NotificationManager.IMPORTANCE_MIN)
            manager.createNotificationChannel(channel)
        }
    }

}