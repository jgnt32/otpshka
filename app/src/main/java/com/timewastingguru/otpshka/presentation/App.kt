package com.timewastingguru.otpshka.presentation

import android.app.Application

class App: Application() {


    companion object {
        lateinit var provider: DependencyProvider
    }

    override fun onCreate() {
        super.onCreate()
        provider = DependencyProvider(this)
    }

}