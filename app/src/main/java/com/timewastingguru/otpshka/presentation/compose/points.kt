package com.timewastingguru.otpshka.presentation.compose

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import com.timewastingguru.otpshka.domain.entities.TransferPoint

@Composable
fun PointsList(liveDate: LiveData<List<TransferPoint>>,
               onPointClick: (TransferPoint) -> Unit) {
    val points: State<List<TransferPoint>> = liveDate.observeAsState(emptyList())

    if (points.value.isEmpty()) {
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxHeight(),
            contentAlignment = Alignment.Center

        ) {
           Text(
               modifier = Modifier.padding(32.dp),
               text = "There are no forwarding points. Press + to create a new one",
               textAlign = TextAlign.Center
           )
        }

    } else {
        LazyColumn {
            items(points.value) { point ->
                PointItem(point = point, onPointClick)
                Divider(color = Color.LightGray, thickness = Dp(0.5f))
            }
        }
    }

}


@Composable
fun PointItem(point: TransferPoint, onPointClick: (TransferPoint) -> Unit) {
    Row {

        Box(modifier = Modifier
            .fillMaxWidth()
            .clickable { onPointClick(point) }
            .padding(16.dp)) {

            Text( text = point.name)

        }
    }

}