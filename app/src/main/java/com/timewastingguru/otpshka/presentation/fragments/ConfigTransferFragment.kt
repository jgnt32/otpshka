package com.timewastingguru.otpshka.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Box
import androidx.compose.material.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.presentation.compose.ConfigState
import com.timewastingguru.otpshka.presentation.compose.ConfigTransfer
import com.timewastingguru.otpshka.presentation.viewmodels.ConfigTransferViewModel

class ConfigTransferFragment: Fragment() {

    companion object {
        val ADDRESS = "address"
    }

    val viewModel by viewModels<ConfigTransferViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getString("address")?.let {
            viewModel.onAddressSet(it)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {

                Scaffold(
                    topBar = { TopAppBar(
                        title = { Text(text = "Config transfer" ) },
                        navigationIcon = { IconButton(
                            onClick = { findNavController().popBackStack() },
                            content = { Icon(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = "kek") }
                        ) }
                    ) },

                    content = {
                        val state by viewModel.configState.observeAsState(ConfigState(null, null))
                        ConfigTransfer(
                            configState = state,
                            onPickPointClick = {
                                   findNavController().navigate(R.id.action_configTransferFragment_to_transferPointsFragment,
                                    Bundle().apply {
                                        putString(
                                            ADDRESS,
                                            viewModel.configState.value?.address
                                        )
                                    }
                               )
                            },
                            onSaveClick = {
                                viewModel.onSaveClick()
                                findNavController().popBackStack()
                            },
                            onDeleteClick = { address, point ->
                                viewModel.onDeleteLink(address, point)
                            },
                            onAddressChanged = { address ->
                                viewModel.onAddressChanged(address)

                            }
                        )
                    }
                )

            }
        }
    }

    override fun onStart() {
        super.onStart()
        findNavController().currentBackStackEntry?.savedStateHandle?.
            getLiveData<Long>("transfer_point")?.
            observe(this) {
                viewModel.onPointAdded(it)
            }
    }

}