package com.timewastingguru.otpshka.presentation.compose

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.domain.entities.TransferPoint

data class ConfigState(
    val address: String?,
    val transferPoints: List<TransferPoint>?
)

@Preview(name = "Config", backgroundColor = 0xFFFFFF, showBackground = true)
@Composable
fun ConfigTransfer(@PreviewParameter(ConfigStatePreviewParameterProvider::class) configState: ConfigState,
                   onPickPointClick: ((address: String?) -> Unit)? = null,
                   onDeleteClick: ((address: String, point: TransferPoint) -> Unit)? = null,
                   onSaveClick: (() -> Unit)? = null, onAddressChanged: ((String) -> Unit)? = null) {

    Column {

        Box(modifier = Modifier.padding(16.dp)) {

            OutlinedTextField(
                value = configState.address ?: "",
                label = { Text(text = "Address")},
                onValueChange =  {kek -> }
            )

        }

        Box(modifier = Modifier.padding(start = 16.dp, top = 8.dp, bottom = 8.dp)) {
            Text(text = "Transfer points", fontWeight = FontWeight.Bold)
        }
        Divider(color = Color.LightGray, thickness = Dp(0.5f))

        LazyColumn {
            items(configState.transferPoints?.size ?: 0) { index ->
                TransferPointItem(transferPoint = configState.transferPoints!![index]) {
                    onDeleteClick?.invoke(
                        configState.address!!,
                        configState.transferPoints[index]
                    )
                }
                Divider(color = Color.LightGray, thickness = Dp(0.5f))
            }

            item {
                AddTransferPointItem{                    onPickPointClick?.invoke(configState.address)
                }
            }
        }

        Box(contentAlignment = Alignment.Center, modifier = Modifier.fillMaxWidth().padding(top = 16.dp)) {
            Button(
                onClick = { onSaveClick?.invoke() },
                content = { Text(text = "Save") },
                enabled = configState.address != null && configState.transferPoints != null && configState.transferPoints.isNotEmpty(),
            )
        }

    }

}

class ConfigStatePreviewParameterProvider: PreviewParameterProvider<ConfigState> {
    override val values: Sequence<ConfigState> = sequenceOf(ConfigState("900", listOf(TransferPoint(1, "my_slack_channel", emptyMap()), TransferPoint(1, "one_more_slack_channel", emptyMap()))))
    override val count = 3
}


@Preview(name = "Add",backgroundColor = 0xFFFFFF, showBackground = true)
@Composable
fun AddTransferPointItem(@PreviewParameter(UnitLamblaParametr::class) onPickPointClick: () -> Unit) {

    Row(modifier = Modifier
        .clickable { onPickPointClick() }
        .padding(16.dp)
        .fillMaxWidth(), horizontalArrangement = Arrangement.Center) {

        Row {

            Text(
                text = "Add point"
            )

            Icon(
                painter = painterResource(id = R.drawable.ic_baseline_add_circle_outline_24),
                contentDescription = "kek",
                modifier = Modifier.padding(horizontal = 16.dp)
            )
        }

    }
}


class UnitLamblaParametr : PreviewParameterProvider<() -> Unit> {
    override val values = sequenceOf({})
}

class StringUnitLamblaParametr : PreviewParameterProvider<(String) -> Unit> {
    override val values = sequenceOf({ _: String ->})
}
class NullableStringUnitLamblaParametr : PreviewParameterProvider<(String?) -> Unit> {
    override val values = sequenceOf({ _: String? ->})
}


@Preview(backgroundColor = 0xFFFFFF, showBackground = true)
@Composable
fun TransferPointItem(@PreviewParameter(TransferPointParametr::class) transferPoint: TransferPoint, onDeleteClick: (() -> Unit)? = null) {
    Row {

        Text(
            text = transferPoint.name,
            modifier = Modifier
                .weight(1f)
                .padding(16.dp)
        )

        IconButton(
            content = { Icon(painter = painterResource(id = R.drawable.ic_baseline_delete_24), contentDescription = "kek") },
            onClick = { onDeleteClick?.invoke() },
        )
    }
}

class TransferPointParametr: PreviewParameterProvider<TransferPoint> {
    override val values = sequenceOf(TransferPoint(1, "slack_channel_2", emptyMap()))

}