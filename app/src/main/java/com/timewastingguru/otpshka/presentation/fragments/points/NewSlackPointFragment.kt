package com.timewastingguru.otpshka.presentation.fragments.points

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.timewastingguru.otpshka.presentation.compose.FormListener
import com.timewastingguru.otpshka.presentation.compose.NewSlackPointForm
import com.timewastingguru.otpshka.presentation.compose.NewSlackState
import com.timewastingguru.otpshka.presentation.viewmodels.NewSlackPointViewModel

class NewSlackPointFragment: Fragment() {

    val viewModel : NewSlackPointViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val formListener = object : FormListener {
            override fun onUrlChanged(url: String) {
                viewModel.onUrlChanged(url)
            }

            override fun onTitleChanged(title: String) {
                viewModel.onTitleChanged(title)
            }

            override fun submit() {
                viewModel.submit()
                findNavController().popBackStack()
            }


        }

        return ComposeView(requireContext()).apply {
            setContent {

                val valid by viewModel.validForm.observeAsState(NewSlackState("", "", false))

                Scaffold(
                    topBar = {
                        TopAppBar(title = { Text(text = "New slack point" ) })
                    },
                    content = { NewSlackPointForm(valid, formListener) },
                )
            }
        }
    }

}