package com.timewastingguru.otpshka.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.presentation.App
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SmsGroupsViewModel: ViewModel() {

    val smsList = MutableLiveData<List<Sms>>()
    val interactor = App.provider.loadSmsInteractor


    fun onPermissionGranted() {
        viewModelScope.launch {
            interactor.loadSms().collect {
                smsList.value = it
            }
        }
    }


}