package com.timewastingguru.otpshka.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.presentation.compose.TransferList
import com.timewastingguru.otpshka.presentation.viewmodels.TransfersViewModel

class TransfersListFragment: Fragment() {

    val viewModel by viewModels<TransfersViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {

                Scaffold(
                    topBar = { TopAppBar(
                        title = { Text(text = "Transfers" ) },
                        navigationIcon = { IconButton(
                            onClick = { findNavController().popBackStack() },
                            content = { Icon(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = "kek") }
                        ) }
                    ) },

                    content = {
                        val list = viewModel.transfers.observeAsState(emptyList())
                        TransferList(list.value)
                    },

                    floatingActionButton =  {
                        FloatingActionButton(
                            onClick = { findNavController().navigate(R.id.action_transfersListFragment_to_configTransferFragment) },
                            content = { Text(text = "+") })
                    }

                )

            }
        }
    }

}