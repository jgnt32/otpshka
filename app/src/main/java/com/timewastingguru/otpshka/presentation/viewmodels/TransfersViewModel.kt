package com.timewastingguru.otpshka.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timewastingguru.otpshka.domain.entities.TransferConfig
import com.timewastingguru.otpshka.presentation.App
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TransfersViewModel: ViewModel() {

    val transfers = MutableLiveData<List<TransferConfig>>()

    init {
        viewModelScope.launch {
            App.provider.transferPointInteractor.provider.getAllLinks().collect {
                transfers.postValue(it)
            }
        }
    }

}