package com.timewastingguru.otpshka.presentation.compose

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

interface FormListener {

    fun onUrlChanged(url: String)

    fun onTitleChanged(title: String)

    fun submit()

}

data class NewSlackState(
    val title: String,
    val url: String,
    val valid: Boolean
)

@Composable
fun NewSlackPointForm(state: NewSlackState, listener: FormListener) {

    Column {

        Box(modifier = Modifier.padding(start = 16.dp, top = 8.dp, end = 16.dp).fillMaxWidth()) {
            OutlinedTextField(
                value = state.title,
                onValueChange = { listener.onTitleChanged(it) },
                label = { Text("Tittle") },
                modifier = Modifier.fillMaxWidth()

            )

        }
        Box(modifier = Modifier.padding(start = 16.dp, top = 8.dp, end = 16.dp).fillMaxWidth()) {
            OutlinedTextField(
                value = state.url,
                onValueChange = {
                    listener.onUrlChanged(it)
                },
                label = { Text("URL") },
                modifier = Modifier.fillMaxWidth()
            )

        }

        Box(modifier = Modifier.padding(16.dp).fillMaxWidth(), contentAlignment = Alignment.Center) {
            Button(
                onClick = { listener.submit() },
                content = {Text(text = "Add")},
                enabled = state.valid
            )
        }




    }

}
