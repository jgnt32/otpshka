package com.timewastingguru.otpshka.presentation

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Telephony
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.timewastingguru.otpshka.R
import java.util.*

class SmsReceiver: BroadcastReceiver() {

    companion object {

        const val CHANNEL_ID = "sms"
    }

    override fun onReceive(context: Context, intent: Intent) {

        val smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent)

        val address = smsMessages[0].displayOriginatingAddress
        val text = smsMessages[0].messageBody
        showNotification(context, address, text)
        App.provider.receiveSmsInteractor.onSmsReceived(
            context = context,
            text = text,
            address = address,
            date = Date(smsMessages[0].timestampMillis)
        )

    }

    fun showNotification(context: Context, address: String, text: String) {

        val pendingIntent: PendingIntent = Intent(context, MainActivity::class.java).let { notificationIntent ->
                PendingIntent.getActivity(context, 0, notificationIntent, 0)
            }

        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notification: Notification = NotificationCompat.Builder(context,
            CHANNEL_ID
        )
            .setContentTitle(address)
            .setContentText(text)
            .setSmallIcon(R.drawable.ic_baseline_textsms_24)
            .setContentIntent(pendingIntent)
            .build()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(manager)
        }

        manager.notify(text.hashCode(), notification)


    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(manager: NotificationManager) {

        if (manager.getNotificationChannel(CHANNEL_ID) == null) {
            val channel = NotificationChannel(CHANNEL_ID, "Displaying sms", NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }
    }

}