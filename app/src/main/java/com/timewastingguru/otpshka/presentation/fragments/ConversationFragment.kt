package com.timewastingguru.otpshka.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.presentation.compose.Conversation
import com.timewastingguru.otpshka.presentation.compose.SmsList
import com.timewastingguru.otpshka.presentation.viewmodels.ConversationViewModel

class ConversationFragment: Fragment() {

    companion object {

        val ADDRESS = "address"

        fun newInstance(address: String): ConversationFragment {
            val fragment = ConversationFragment()
            val args = Bundle()
            args.putString(ADDRESS, address)
            fragment.arguments = args
            return fragment
        }

    }

    val viewModel : ConversationViewModel by viewModels ()

    lateinit var address: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        address = requireArguments().getString(ADDRESS)!!
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadConversation(address)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                Conversation(viewModel.smsList, address) { findNavController().popBackStack() }
            }
        }
    }


}