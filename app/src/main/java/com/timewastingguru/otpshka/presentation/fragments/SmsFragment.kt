package com.timewastingguru.otpshka.presentation.fragments

import android.app.role.RoleManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.material.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.presentation.compose.SmsGroupList
import com.timewastingguru.otpshka.presentation.viewmodels.SmsGroupsViewModel

class SmsFragment: Fragment() {

    companion object {
        val PERMISSIONS_REQUEST_CODE = 2211
        val REQUEST_CODE_DEFAULT = 1122
    }

    val smsGroupsViewModel: SmsGroupsViewModel by viewModels ()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestSmsPermissions()
        smsGroupsViewModel.smsList.observe(this) { smsList ->
            Log.e("MainActivity", "smsList ${smsList.size}")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = { Text(text = "OTPshka" ) },
                        )
                    },

                    floatingActionButton = {
                           FloatingActionButton(
                               onClick = {
                                     findNavController().navigate(R.id.action_smsFragment_to_transfersListFragment)
                               },
                               content = {
                                   Icon(
                                       painter = painterResource(id = R.drawable.ic_settings),
                                       contentDescription = null,
                                       tint = Color.White,
                                       modifier = Modifier.requiredSize(25.dp))
                               }
                           )
                    },
                    content = {
                        val onSmsClick: (Sms) -> Unit = {
                            findNavController().navigate(R.id.action_smsFragment_to_conversationFragment,
                                Bundle().apply {
                                    putString(
                                        ConversationFragment.ADDRESS,
                                        it.address
                                    )
                                })
                        }

                        val onConfigClick: (Sms) -> Unit = {

                            findNavController().navigate(R.id.action_smsFragment_to_configTransferFragment,
                                Bundle().apply {
                                putString(
                                    ConfigTransferFragment.ADDRESS,
                                    it.address
                                )
                            })
                        }
                        val sms = smsGroupsViewModel.smsList.observeAsState(emptyList())
                        SmsGroupList(sms.value, onSmsClick, onConfigClick)
                    }
                )

            }
        }
    }

    private fun requestSmsPermissions() {
        val permissions =
            arrayOf(
                android.Manifest.permission.READ_SMS,
                android.Manifest.permission.RECEIVE_SMS
            )

        val checkResult = context?.run {  ContextCompat.checkSelfPermission(this, permissions[0]) } ?: return

        if (checkResult != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(permissions, PERMISSIONS_REQUEST_CODE)
        } else {
//            showDefaultSmsDialog()
            smsGroupsViewModel.onPermissionGranted()
        }

    }

    fun showDefaultSmsDialog() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            val roleManager = context?.getSystemService(RoleManager::class.java) as RoleManager

            if (roleManager.isRoleHeld(RoleManager.ROLE_SMS)) {
                return
            }

            val intent = roleManager.createRequestRoleIntent(RoleManager.ROLE_SMS)
            startActivityForResult(intent, 42389)
        } else {

            if (Telephony.Sms.getDefaultSmsPackage(context).equals(context?.packageName)) {
                return
            }

            val intent = Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT)
            intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, context?.packageName)
            startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            showDefaultSmsDialog()
            smsGroupsViewModel.onPermissionGranted()
        }

    }

}