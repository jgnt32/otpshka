package com.timewastingguru.otpshka.presentation.compose

import android.widget.EditText
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.domain.entities.Sms
import java.util.*

@Preview(backgroundColor = 0xFFFF, showBackground = true, showSystemUi = true)
@Composable
fun Conversation(@PreviewParameter(SmsCoversationPreviewParameterProvider::class) list: LiveData<List<Sms>>, address: String = "900", back: (() -> Unit)? = null) {

    Scaffold {

        Column {
            TopAppBar(
                title = { Text(text = address ) },
                navigationIcon = { IconButton(
                    onClick = { back?.invoke() },
                    content = { Icon(painter = painterResource(id = R.drawable.ic_baseline_arrow_back_24), contentDescription = "kek") }
                ) }
            )
            SmsList(list)
            SmsEditField()
        }
    }

}


@Composable
fun SmsList(sms: LiveData<List<Sms>>) {
    val smsList: State<List<Sms>> = sms.observeAsState(emptyList())
    LazyColumn {
        items(smsList.value) { sms ->
            SmsItem(sms)
            Divider(color = Color.LightGray, thickness = Dp(0.5f))
        }
    }

}

@Composable
fun SmsEditField() {
    Row() {
        Box(Modifier
            .border(1.dp, Color.Black)
            .width(IntrinsicSize.Max)
            .height(50.dp)) {
           TextField(value = "Kekek", onValueChange = {})

        }
    }
}


class SmsCoversationPreviewParameterProvider : PreviewParameterProvider<LiveData<List<Sms>>> {
    override val values = sequenceOf(MutableLiveData(listOf(Sms(1, "Hi m8!", "900", Date()), Sms(12, "Hello dude!", "900", Date()))))

}