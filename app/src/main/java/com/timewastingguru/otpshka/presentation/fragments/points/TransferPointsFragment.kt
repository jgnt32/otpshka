package com.timewastingguru.otpshka.presentation.fragments.points

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.presentation.compose.PointsList
import com.timewastingguru.otpshka.presentation.viewmodels.TransferPointsViewModel

class TransferPointsFragment: Fragment() {

    val viewModel: TransferPointsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setAddress(requireArguments().getString("address")!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                Scaffold(
                    topBar = {
                        TopAppBar(title = { Text(text = "Pick point" ) })
                    },
                    content = {
                        PointsList(liveDate = viewModel.points) { point ->
                            findNavController().previousBackStackEntry?.savedStateHandle?.set("transfer_point", point.id)
                            findNavController().popBackStack()
                        }
                    },
                    floatingActionButton = {
                        FloatingActionButton(
                            onClick = {
                                findNavController().navigate(R.id.action_transferPointsFragment_to_newSlackPointFragment) },
                            content = { Text(text = "+")}
                        )

                    }

                )
            }
        }

    }

}