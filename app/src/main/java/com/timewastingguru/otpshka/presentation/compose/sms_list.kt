package com.timewastingguru.otpshka.presentation.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.domain.entities.Sms
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun SmsGroupItem(sms: Sms, onClick: (Sms) -> Unit, onConfigClick: (Sms) -> Unit){

    Row(modifier = Modifier.fillMaxWidth()) {
        Column(
            modifier = Modifier
                .clickable { onClick(sms) }
                .weight(2f)) {
            Text(
                text = sms.address,
                fontFamily = FontFamily.SansSerif,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)

            )

            Text(text = sms.text,
                modifier = Modifier.padding(
                    start = Dp(16f),
                    end = Dp(16f),
                    bottom = Dp(8f)
                )
            )


        }
        IconButton(
            content = {
                Image(
                    painter = painterResource(id = R.drawable.ic_settings),
                    contentDescription = null,
                    modifier = Modifier.requiredSize(25.dp)
                )
            },
            onClick = { onConfigClick(sms) },
            modifier = Modifier
                .align(Alignment.CenterVertically)
        )



    }
}

@Preview(backgroundColor = 0xFFFFFF, showBackground = true)
@Composable
fun SmsGroupList(@PreviewParameter(SmsGroupPreviewParameterProvider::class) sms: List<Sms>,
                 onClick: ((Sms) -> Unit)? = null,
                 onConfigClick: ((Sms) -> Unit)? = null) {
    LazyColumn {
        items(sms) { sms ->
            SmsGroupItem(sms, onClick ?: {}, onConfigClick ?: {})
            Divider(color = Color.LightGray, thickness = Dp(0.5f))
        }
    }
}

val dateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.forLanguageTag("ru"))

@Composable
fun SmsItem(sms: Sms) {

    Column {
        Text(
            text = sms.text,
            fontFamily = FontFamily.SansSerif,
            modifier = Modifier.padding(Dp(8f))
        )

        Text(text = dateFormat.format(sms.date),
            modifier = Modifier.padding(
                start = Dp(16f),
                end = Dp(16f),
                bottom = Dp(8f)
            )
        )
    }

}


class SmsGroupPreviewParameterProvider : PreviewParameterProvider<List<Sms>> {
    override val values = sequenceOf(listOf(Sms(1, "Hi m8!", "900", Date()), Sms(12, "Hello dude!", "900", Date())))

}