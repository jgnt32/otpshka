package com.timewastingguru.otpshka.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timewastingguru.otpshka.domain.entities.TransferType
import com.timewastingguru.otpshka.domain.entities.slack.SlackChannel
import com.timewastingguru.otpshka.presentation.App
import com.timewastingguru.otpshka.presentation.compose.NewSlackState
import kotlinx.coroutines.launch

class NewSlackPointViewModel: ViewModel() {

//    val channels: MutableLiveData<List<SlackChannel>> = MutableLiveData()
//
//    fun getChannels(token: String) {
//        viewModelScope.launch{
//            val slackChannels = App.provider.providers.slackChannels.getChannels(token)
//            channels.postValue(slackChannels)
//
//        }
//    }

    private val _validForm = MutableLiveData<NewSlackState>()
    val validForm = _validForm

    var url: String? = null

    var title: String? = null

    fun onUrlChanged(url: String) {
        this.url = url
        invalidate()
    }

    fun onTitleChanged(title: String){
        this.title = title
        invalidate()
    }

    fun submit() {
        App.provider.transferPointInteractor.add(title!!, TransferType.SLACK, hashMapOf<String, Any>().apply { put("url", url!!) })
    }

    fun invalidate() {
        val urlValid = url?.isNotBlank() ?: false
        val titleValid = title?.isNotBlank() ?: false
        _validForm.value = NewSlackState(title ?: "", url ?: "", urlValid && titleValid)
    }


}