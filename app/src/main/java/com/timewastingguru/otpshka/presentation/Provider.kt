package com.timewastingguru.otpshka.presentation

import android.content.Context
import androidx.room.Room
import com.timewastingguru.otpshka.data.points.RetrofitTransferProvider
import com.timewastingguru.otpshka.data.points.slack.RetrofitSlackChannelProvider
import com.timewastingguru.otpshka.data.sms.AppDatabase
import com.timewastingguru.otpshka.data.sms.RoomSmsConversationsProvider
import com.timewastingguru.otpshka.data.sms.RoomTransferPointProvider
import com.timewastingguru.otpshka.domain.interactors.*
import com.timewastingguru.otpshka.domain.providers.SlackChannelsProvider
import com.timewastingguru.otpshka.domain.providers.SmsConversationsProvider
import com.timewastingguru.otpshka.domain.providers.TransferPointProvider

class DependencyProvider (context: Context) {

    private val db = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "otpshka-db"
    ).build()


    class Providers {

        val slackChannels: SlackChannelsProvider = RetrofitSlackChannelProvider()

    }

    val providers = Providers()


    private val smsProvider = SmsProvider(context)
    private val smsConversationsProvider: SmsConversationsProvider = RoomSmsConversationsProvider(db)
    private val transferPointProvider: TransferPointProvider = RoomTransferPointProvider(db.transfersDao(), db.smsDao())

    val loadSmsInteractor = LoadSmsInteractor(smsConversationsProvider, smsProvider)

    val receiveSmsInteractor = ReceiveSmsInteractor(smsConversationsProvider, transferPointProvider)

    val conversationInteractor = LoadConversationInteractor(smsConversationsProvider)

    val transferPointInteractor = TransferPointsInteractor(transferPointProvider)

    val sendAllTransfersIteractor = SendAllTransfersInteractor(transferPointProvider, RetrofitTransferProvider())


}