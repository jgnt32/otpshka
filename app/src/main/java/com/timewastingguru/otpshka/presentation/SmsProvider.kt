package com.timewastingguru.otpshka.presentation

import android.content.Context
import android.provider.Telephony
import com.timewastingguru.otpshka.domain.entities.Sms
import java.util.*

class SmsProvider(val context: Context) {


    fun readAllSms(): List<Sms> {
        val result = arrayListOf<Sms>()
        val cursor = context.contentResolver.query(
            Telephony.Sms.CONTENT_URI,
            arrayOf(Telephony.Sms.Inbox.ADDRESS, Telephony.Sms.Inbox.BODY, Telephony.Sms.Inbox.DATE),
            null,
            null,
            Telephony.Sms.Inbox.DEFAULT_SORT_ORDER
        )


        val addressColumn = cursor?.getColumnIndex(Telephony.Sms.ADDRESS) ?: return result
        val bodyColumn = cursor.getColumnIndex(Telephony.Sms.BODY)
        val dateColumn = cursor.getColumnIndex(Telephony.Sms.DATE)

        cursor.use { c ->

            while (c.moveToNext()) {
                val address = c.getString(addressColumn)
                val body = c.getString(bodyColumn)
                val date = c.getLong(dateColumn)

                result.add(Sms(0, body, address, Date(date)))
            }
        }

        return result
    }

}