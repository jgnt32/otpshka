package com.timewastingguru.otpshka.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.timewastingguru.otpshka.domain.entities.TransferPoint
import com.timewastingguru.otpshka.presentation.App
import com.timewastingguru.otpshka.presentation.compose.ConfigState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ConfigTransferViewModel: ViewModel() {

    val configState = MutableLiveData<ConfigState>(ConfigState(null, null))

    fun onAddressSet(address: String) {
        configState.postValue(ConfigState(address, configState.value?.transferPoints))
        viewModelScope.launch {
            App.provider.transferPointInteractor.getByAddress(address).collect {
                configState.postValue(ConfigState(address, it))
            }
        }
    }

    fun onAddressChanged(address: String) {
        configState.postValue(ConfigState(address, configState.value?.transferPoints))
    }

    fun onPointAdded(pointId: Long) {
        viewModelScope.launch {
            App.provider.transferPointInteractor.getById(pointId).collect {
                val newPoints = ArrayList(configState.value?.transferPoints ?: ArrayList()).apply { add(it) }
                configState.postValue(ConfigState(configState.value?.address, newPoints))
            }
        }
    }

    fun onSaveClick() {
        viewModelScope.launch {
            val state = configState.value
            App.provider.transferPointInteractor.saveLinks(state!!.address!!, state.transferPoints!!)
        }
    }

    fun onDeleteLink(address: String, point: TransferPoint) {
        val newPoints = ArrayList(configState.value?.transferPoints ?: ArrayList()).apply { remove(point) }
        configState.postValue(ConfigState(configState.value?.address, newPoints))
    }

}