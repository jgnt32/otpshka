package com.timewastingguru.otpshka.presentation.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.timewastingguru.otpshka.R
import com.timewastingguru.otpshka.domain.entities.TransferConfig

@Composable
fun TransferList(list: List<TransferConfig>) {

    if (list.isEmpty()) {
        Box(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxHeight(),
            contentAlignment = Alignment.Center

        ) {
            Text(
                modifier = Modifier.padding(32.dp),
                text = "No forwarding configured. Press + to config new one",
                textAlign = TextAlign.Center
            )
        }

    } else {

        LazyColumn {
            items(list) { config ->
                TransferItem(config = config)
                Divider(color = Color.LightGray, thickness = Dp(0.5f))
            }
        }

    }


}

@Composable
fun TransferItem(config: TransferConfig) {

    Column(Modifier.padding(16.dp)) {

        Text(text = config.address)

        Image(
            painter = painterResource(id = R.drawable.ic_arrow_forward_24),
            contentDescription = null )

        Column{

            config.transferPoints.forEach { point ->
                Text(text = point.name)
            }
        }


    }


}