package com.timewastingguru.otpshka.data.sms

import com.timewastingguru.otpshka.data.sms.entities.RoomSms
import com.timewastingguru.otpshka.domain.entities.Conversation
import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.domain.providers.SmsConversationsProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import java.util.*

class RoomSmsConversationsProvider(val appDatabase: AppDatabase): SmsConversationsProvider {

    override fun getSmsGroupedByAddress(): Flow<List<Sms>> {
        return appDatabase.smsDao().getSmsPreview().map { roomSmsList ->
            roomSmsList.map { sms -> Sms(id = sms.id, text = sms.text, address = sms.address, date = sms.date) }
        }
    }

    override fun getAllSmsByAddress(address: String): Flow<List<Sms>> {
        return appDatabase.smsDao().getSmsListByAddress(address).map { roomSmsList ->
            roomSmsList.map { sms -> Sms(sms.id, sms.text, address = sms.address, date = sms.date) }
        }
    }

    override suspend fun importSms(list: List<Sms>) {
        appDatabase.smsDao().insertAllSms(list.map {sms ->
            RoomSms(sms.text, sms.date, sms.address)
        })
    }

    override suspend fun getSmsById(id: Long): Sms {
        val roomSms = appDatabase.smsDao().getSmsById(id)
        return Sms(roomSms.id, roomSms.text, roomSms.address, roomSms.date)
    }

    override fun count(): Flow<Int> {
        return appDatabase.smsDao().count()
    }

    override suspend fun onSmsReceived(address: String, text: String, date: Date): Long {
        return appDatabase.smsDao().insertSms(RoomSms(text, date, address))
    }
}