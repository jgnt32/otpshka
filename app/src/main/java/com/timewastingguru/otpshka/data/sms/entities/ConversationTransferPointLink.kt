package com.timewastingguru.otpshka.data.sms.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "link")
data class ConversationTransferPointLink(
    val address: String,
    val transferPointId: Long,
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

