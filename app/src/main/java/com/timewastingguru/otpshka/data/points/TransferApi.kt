package com.timewastingguru.otpshka.data.points

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

interface TransferApi {

    @POST
    suspend fun sendSlackMessage(@Url url: String, @Body body: RequestBody): ResponseBody

}