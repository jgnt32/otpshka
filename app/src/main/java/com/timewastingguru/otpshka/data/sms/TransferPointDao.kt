package com.timewastingguru.otpshka.data.sms

import androidx.room.*
import com.timewastingguru.otpshka.data.sms.entities.ConversationTransferPointLink
import com.timewastingguru.otpshka.data.sms.entities.RoomTransfer
import com.timewastingguru.otpshka.data.sms.entities.RoomTransferPoint
import com.timewastingguru.otpshka.domain.entities.TransferPoint
import kotlinx.coroutines.flow.Flow

@Dao
interface TransferPointDao {

    @Query("SELECT * FROM transfer_points")
    fun getAll(): Flow<List<RoomTransferPoint>>

    @Query("SELECT * FROM transfer_points WHERE id NOT IN (:ids)")
    fun getExcept(ids: List<Long>):Flow<List<RoomTransferPoint>>

    @Query("SELECT * FROM transfer_points WHERE id = :id")
    suspend fun getPointById(id: Long): RoomTransferPoint

    @Insert
    suspend fun insertAll(vararg transferPoint: RoomTransferPoint)

    @Query("DELETE FROM transfer_points WHERE id = :id")
    suspend fun deletePoint(id: Long)

    @Insert
    suspend fun insertLink(vararg link: ConversationTransferPointLink)

    @Query("DELETE FROM link WHERE id = :linkId")
    suspend fun deleteLink(linkId: Long)

    @Query("SELECT * FROM link WHERE address = :address AND transferPointId = :pointId")
    suspend fun getLink(address: String, pointId: Long): ConversationTransferPointLink?

    @Query("DELETE FROM link WHERE address = :address")
    suspend fun deleteAllLinksByAddress(address: String)

    @Query("SELECT * FROM link GROUP BY address")
    fun getAllLinks(): Flow<List<ConversationTransferPointLink>>

    @Insert
    suspend fun addTransfer(roomTransfer: RoomTransfer): Long

    @Query("SELECT * FROM transfers")
    fun getAllTransfers(): Flow<List<RoomTransfer>>

    @Query("DELETE FROM transfers WHERE id = :id")
    suspend fun deleteTransfer(id: Long)

    @Query("DELETE FROM transfers WHERE transferPointId = :pointId")
    suspend fun deleteAllTransfersByPoint(pointId: Long)

    @Query("SELECT * FROM transfer_points where id in (SELECT transferPointId FROM link WHERE address = :address)")
    fun getPointsByAddress(address: String): Flow<List<RoomTransferPoint>>

}