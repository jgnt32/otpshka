package com.timewastingguru.otpshka.data.sms.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "messages")
data class RoomSms(
    val text: String,
    val date: Date,
    val address: String
    ) {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L

}