package com.timewastingguru.otpshka.data.sms.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "conversations")
data class RoomConversation(
    val address: String) {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
}