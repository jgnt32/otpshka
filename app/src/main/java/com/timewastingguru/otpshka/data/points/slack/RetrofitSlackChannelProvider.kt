package com.timewastingguru.otpshka.data.points.slack

import com.squareup.moshi.Moshi
import com.timewastingguru.otpshka.domain.entities.slack.SlackChannel
import com.timewastingguru.otpshka.domain.providers.SlackChannelsProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitSlackChannelProvider: SlackChannelsProvider {

    val moshi = Moshi.Builder()
        .build()

    private val logging =  HttpLoggingInterceptor().apply{
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    private val client = OkHttpClient.Builder()
        .addInterceptor(logging)
        .build();

    private val retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl("https://slack.com")
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    val api = retrofit.create(SlackApi::class.java)


    override suspend fun getChannels(token: String): List<SlackChannel> {
        return api.getChannels(token).channels.map { SlackChannel(it.name, it.id) }
    }


}