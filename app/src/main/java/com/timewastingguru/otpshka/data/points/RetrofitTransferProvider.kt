package com.timewastingguru.otpshka.data.points

import android.util.Log
import com.squareup.moshi.Moshi
import com.timewastingguru.otpshka.domain.entities.Sms
import com.timewastingguru.otpshka.domain.entities.TransferPoint
import com.timewastingguru.otpshka.domain.providers.TransferProvider
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RetrofitTransferProvider: TransferProvider {

    val moshi = Moshi.Builder()
        .build()

    private val logging = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    private val client = OkHttpClient.Builder()
        .addInterceptor(logging)
        .build();

    private val retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl("https://slack.com")
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

    val api = retrofit.create(TransferApi::class.java)


    override suspend fun transfer(sms: Sms, transferPoint: TransferPoint) {


        val text = "{\"text\": \"${sms.address}:\n${sms.text}\"}"

        val body = RequestBody.create("application/json".toMediaTypeOrNull(), text)

        val response = api.sendSlackMessage(transferPoint.payload["url"] as String, body)

         Log.e("TransferProvider", "transfer $response")

    }
}