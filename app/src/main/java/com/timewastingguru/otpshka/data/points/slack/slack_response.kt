package com.timewastingguru.otpshka.data.sms.entities.slack

data class Channels (val channels: List<Channel>)
data class Channel(val id: String, val name: String)