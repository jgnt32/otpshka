package com.timewastingguru.otpshka.data.points.slack

import com.timewastingguru.otpshka.data.sms.entities.slack.Channels
import retrofit2.http.Header
import retrofit2.http.POST

interface SlackApi {

    @POST("/conversations.list")
    fun getChannels( @Header("Authorization") token: String): Channels

}
