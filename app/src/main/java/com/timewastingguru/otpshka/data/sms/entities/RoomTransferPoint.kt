package com.timewastingguru.otpshka.data.sms.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.timewastingguru.otpshka.domain.entities.TransferType

@Entity(tableName = "transfer_points")
data class RoomTransferPoint(
    val name: String,
    val type: TransferType,
    var payload: String? = null
) {

    @PrimaryKey(autoGenerate = true)
    var id = 0L
}

