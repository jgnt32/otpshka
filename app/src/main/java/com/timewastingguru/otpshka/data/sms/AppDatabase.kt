package com.timewastingguru.otpshka.data.sms

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.timewastingguru.otpshka.data.sms.entities.*

@Database(entities = [
    RoomConversation::class,
    RoomSms::class,
    RoomTransferPoint::class,
    ConversationTransferPointLink::class,
    RoomTransfer::class], version = 1)

@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun smsDao(): SmsDao

    abstract fun transfersDao(): TransferPointDao

}