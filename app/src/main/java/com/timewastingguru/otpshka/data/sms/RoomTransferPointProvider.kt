package com.timewastingguru.otpshka.data.sms

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.timewastingguru.otpshka.data.sms.entities.ConversationTransferPointLink
import com.timewastingguru.otpshka.data.sms.entities.RoomTransfer
import com.timewastingguru.otpshka.data.sms.entities.RoomTransferPoint
import com.timewastingguru.otpshka.domain.entities.*
import com.timewastingguru.otpshka.domain.providers.TransferPointProvider
import kotlinx.coroutines.flow.*

class RoomTransferPointProvider(val trasferPointDao: TransferPointDao, val smsDao: SmsDao): TransferPointProvider {

    val moshi = Moshi.Builder().build()

    val moshiType =
        Types.newParameterizedType(Map::class.java, String::class.java, Any::class.java)

    val adapter = moshi.adapter<Map<String, Any>>(moshiType)

    override fun getExcept(address: String): Flow<List<TransferPoint>> {
        return trasferPointDao.getPointsByAddress(address).flatMapConcat {  points ->
            trasferPointDao.getExcept(points.map { it.id })
        }.map { it.mapToDomain() }

    }

    private fun List<RoomTransferPoint>.mapToDomain(): List<TransferPoint> {
        return this.map { point ->

            var payload: Map<String, Any>

            val payloadString = point.payload
            payload = if (payloadString != null) {
                adapter.fromJson(payloadString) ?: emptyMap()
            } else {
                emptyMap()
            }

            TransferPoint(point.id, point.name, payload)
        }

    }

    override fun getAll(): Flow<List<TransferPoint>> {
        return trasferPointDao.getAll().map { list -> list.mapToDomain() }
    }

    override fun getById(id: Long): Flow<TransferPoint> {
        return flow {
            emit(trasferPointDao.getPointById(id))
        }.map {
            val payload = mapPayload(it)

            TransferPoint(it.id, it.name, payload)
        }
    }

    private fun mapPayload(it: RoomTransferPoint): Map<String, Any> {
        val payload = if (it.payload != null) {
            adapter.fromJson(it.payload) ?: emptyMap()
        } else {
            emptyMap()
        }
        return payload
    }

    override suspend fun add(title: String, type: TransferType, payload: Map<String, Any>) {


        trasferPointDao.insertAll(RoomTransferPoint(title, type, adapter.toJson(HashMap(payload))))
    }

    override suspend fun delete(point: TransferPoint) {
        trasferPointDao.deletePoint(point.id)
    }

    override suspend fun addLink(address: String, point: TransferPoint) {
        val link = ConversationTransferPointLink(address, point.id)
        trasferPointDao.insertLink(link)

    }

    override suspend fun breakAllLinks(address: String) {
        trasferPointDao.deleteAllLinksByAddress(address = address)
    }


    override suspend fun breakLink(address: String, point: TransferPoint) {
        val roomPoint = trasferPointDao.getPointById(point.id)
        val link = trasferPointDao.getLink(address, roomPoint.id) ?: return
        trasferPointDao.deleteLink(link.id)
    }

    override suspend fun getAllTransfers(): Flow<List<Transfer>> {

        return trasferPointDao.getAllTransfers().map {
            it.map { roomTransfer ->
                val sms = smsDao.getSmsById(roomTransfer.smsId).run {
                    Sms(id = id, address = address, text = text, date = date)
                }

                val point = trasferPointDao.getPointById(roomTransfer.transferPointId)
                val mappedPoint = TransferPoint(point.id, point.name, mapPayload(point))
                Transfer(roomTransfer.id, sms, mappedPoint)
            }

        }
    }

    override fun getPointsByAddress(address: String): Flow<List<TransferPoint>> {
        return trasferPointDao.getPointsByAddress(address).map {
            it.map {
                TransferPoint(it.id, it.name, mapPayload(it))
            }
        }
    }

    override suspend fun addTransfer(sms: Sms, transferPoint: TransferPoint) {
        trasferPointDao.addTransfer(RoomTransfer(sms.id, transferPoint.id))
    }

    override suspend fun deleteTransfer(id: Long) {
        trasferPointDao.deleteTransfer(id)
    }

    override fun getAllLinks(): Flow<List<TransferConfig>> {
        return trasferPointDao.getAllLinks().map { links ->
            val result = arrayListOf<TransferConfig>()

            links.forEach { link ->
                val transferPoint = getPointsByAddress(link.address).first()
                result.add(TransferConfig(link.address, transferPoint))
            }

            result
        }
    }
}