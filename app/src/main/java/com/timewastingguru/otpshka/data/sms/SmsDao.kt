package com.timewastingguru.otpshka.data.sms

import androidx.room.*
import com.timewastingguru.otpshka.data.sms.entities.RoomConversation
import com.timewastingguru.otpshka.data.sms.entities.RoomSms
import kotlinx.coroutines.flow.Flow

@Dao
interface SmsDao {

    @Query("SELECT * FROM conversations")
    fun getAll(): Flow<List<RoomConversation>>

    @Query("SELECT * FROM conversations WHERE id = :id")
    fun getConversation(id: Long): Flow<RoomConversation>

    @Insert
    @Transaction
    fun insertConversationWithSms(conversation: RoomConversation, sms: List<RoomSms>)

    @Query("SELECT * FROM messages WHERE address = :address ORDER BY date desc")
    fun getSmsListByAddress(address: String): Flow<List<RoomSms>>

    @Query("SELECT * FROM messages WHERE id = :id")
    suspend fun getSmsById(id: Long): RoomSms

    @Insert
    suspend fun insertConversation(vararg conversation: RoomConversation)

    @Insert
    suspend fun insertSms(sms: RoomSms): Long

    @Insert
    suspend fun insertAllSms(sms: List<RoomSms>)

    @Query("SELECT * FROM messages GROUP BY address ORDER BY date DESC")
    fun getSmsPreview(): Flow<List<RoomSms>>

    @Insert
    suspend fun insertAll(sms: List<RoomConversation>)

    @Insert
    suspend fun insertConversation(sms: RoomConversation)

    @Query("SELECT COUNT(*) FROM conversations where address = :address LIMIT 1")
    suspend fun contain(address: String): Int

    @Delete
    suspend fun delete(sms: RoomConversation)

    @Query("SELECT COUNT(*) FROM conversations")
    fun count(): Flow<Int>



}