package com.timewastingguru.otpshka.data.sms.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transfers")
class RoomTransfer(
    val smsId: Long,
    val transferPointId: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id = 0L
}
