package com.timewastingguru.otpshka.data.points

data class SlackRequestBody(val text: String)